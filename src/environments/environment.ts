// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  FIREBASE_CONFIG:{
    apiKey: "AIzaSyAcNo6ALeAyA21lGbnUSBebuZW5t4pWamQ",
    authDomain: "sensormonitoring-10595.firebaseapp.com",
    projectId: "sensormonitoring-10595",
    storageBucket: "sensormonitoring-10595.appspot.com",
    messagingSenderId: "197642769915",
    appId: "1:197642769915:web:f8b7cd5fbd03471d76275e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
