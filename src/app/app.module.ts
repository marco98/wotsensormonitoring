import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import {HttpClientModule} from '@angular/common/http'
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { GraphQLModule } from './graphql.module';
import { Apollo } from 'apollo-angular';
import { ApolloClient } from '@apollo/client/core';
import { SelectSourcePage } from './select-source/select-source.page';
import { SensorDetailPage } from './list-sensor/sensor-detail/sensor-detail.page';
import {AngularFireModule} from "@angular/fire"
import {environment} from "src/environments/environment"
import {AngularFirestoreModule} from "@angular/fire/firestore"
import {BackgroundMode} from '@ionic-native/background-mode/ngx'

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(),AppRoutingModule,HttpClientModule, GraphQLModule,AngularFireModule.initializeApp(environment.FIREBASE_CONFIG),AngularFirestoreModule],
  providers: [
    StatusBar,
    SplashScreen,
    BackgroundMode,
    SensorDetailPage,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
