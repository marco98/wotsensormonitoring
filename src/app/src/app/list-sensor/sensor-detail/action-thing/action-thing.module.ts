import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActionThingPageRoutingModule } from './action-thing-routing.module';

import { ActionThingPage } from './action-thing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActionThingPageRoutingModule
  ],
  declarations: [ActionThingPage]
})
export class ActionThingPageModule {}
