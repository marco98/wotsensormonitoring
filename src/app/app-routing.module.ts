import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  {
    path: '',
    redirectTo: 'select-source',
    pathMatch: 'full'
  },
  {
    path: 'select-source',
    loadChildren: () => import('./select-source/select-source.module').then( m => m.SelectSourcePageModule)
  },
  {
    path: 'list-sensor',
    loadChildren: () => import('./list-sensor/list-sensor.module').then( m => m.ListSensorPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
