import { Component } from '@angular/core';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { LoadingController, Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { UtilityService } from './utility.service';
import { SensorDetail } from './list-sensor/list-sensor-model';
import { SensorResults } from './models/sensorResults.model';
declare let Wot: any
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  private myUrl:string[]
  private mySensor:SensorDetail[]
  keyCred: any;
  sensorresults: any;
  db: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private backgroundMode: BackgroundMode,
    private utilityservice:UtilityService,
    private loadingCntrl:LoadingController,
    private toastCtrl:ToastController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      /*if (this.utilityservice.getAllSensor()!=null){
        if(this.platform.is('android')){
      this.backgroundMode.setDefaults({ silent: true });
      this.backgroundMode.enable();
      this.backgroundMode.on('enable').subscribe(() => { 
        setInterval(() => {
          this.getData();
        }, 5000);
      });
    }
  }*/
  this.platform.pause.subscribe(() => {
    //inside the app
    //what you need to do
    this.getData()
});
      this.platform.resume.subscribe(() => {
    //outside the app
    //what you need to do
    this.getData()
});
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    
   // this.backgroundMode.enable();
  })
}


getData(){

this.mySensor=this.utilityservice.getAllSensor()
for (let i = 0; i < this.mySensor.length; i++){
    this.myUrl.push(this.mySensor[i].url)
}
var servient = new Wot.Core.Servient();
servient.addClientFactory(new Wot.Http.HttpsClientFactory());

for (let i = 0; i < this.myUrl.length; i++){
  var sensorname=this.mySensor.find((s)=>s.url===this.myUrl[i]);
  this.keyCred="urn:dev:"+sensorname.titolo
servient.addCredentials({
  [this.keyCred]: {
      "username": "user",
      "password": "pass"  // if you copy these and don't change them, don't claim you were "hacked"
  }
});
var helpers = new Wot.Core.Helpers(servient);
servient.start().then((thingFactory)=> {
  helpers.fetch(this.myUrl[i]).then((td) =>{
    thingFactory.consume(td)
  .then((thing) => {
    console.log(thing);
    td=thing.getThingDescription();
    this.utilityservice.setcurrentTd(td);
    this.utilityservice.setcurrentThing(thing);
    for ( let property in td.properties ) {
      if (td.properties.hasOwnProperty(property)) {
        if((this.utilityservice.getcurrentTd()['properties'][property]['forms'][0]['htv:methodName'])!='PUT'){
          this.utilityservice.getcurrentThing().readProperty(property)
				.then(res =>{
         // console.log("DATI DEL SENSORE"+res['sosa:hasResult'].x),
          /*//dati reali
          this.sensorresults.sensorName=this.sensorName,
          this.sensorresults.datetime=new Date(),
          this.sensorresults.sensorType=property,
          this.sensorresults.sensorNameType=this.sensorName+property,
          this.sensorresults.x=res['sosa:hasResult'].x,
          this.sensorresults.y=res['sosa:hasResult'].y,
          this.sensorresults.z=res['sosa:hasResult'].z,
          */
         //dati mock
         this.sensorresults.sensorName=sensorname,
         this.sensorresults.datetime=new Date(),
         this.sensorresults.sensorType=property,
         this.sensorresults.sensorNameType=sensorname+property,
         this.sensorresults.x=Math.random(),
         this.sensorresults.y=Math.random(),
         this.sensorresults.z=Math.random(),
         this.addtoPouchDB(this.sensorresults)
				.catch(err => window.alert("error: cannot send data to database " + err))

        })
      }
        }
      }}
      
    );
  }).catch((error) => {
    window.alert("Error, thing description not fetchable:" + error)
  })
})
}
}

async addtoPouchDB(sensorresults:SensorResults){
  this.db=new PouchDB('sensorresults');
let loader=this.loadingCntrl.create({
  message:"Please wait..."
});
  (await loader).present();
  try{
    await this.db.post(sensorresults);

  }catch(e){
    this.showToast(e)
  }
  (await loader).dismiss();

}
  showToast(message: string) {
    this.toastCtrl.create({
      message:message,
      duration:3000
    }).then(toastData=>toastData.present());
  }


}
