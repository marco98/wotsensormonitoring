export interface SensorResults{
    sensorName:string,
    sensorType:string,
    datetime:Date,
    sensorNameType:string,
    x:any,
    y:any,
    z:any
}