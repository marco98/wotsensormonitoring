import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListSensorPage } from './list-sensor.page';

describe('ListSensorPage', () => {
  let component: ListSensorPage;
  let fixture: ComponentFixture<ListSensorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSensorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListSensorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
