import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListSensorPage } from './list-sensor.page';

const routes: Routes = [
  {
    path: '',
    component: ListSensorPage
  },
  {
    path: 'sensor-detail/:sensorId',
    loadChildren: () => import('./sensor-detail/sensor-detail.module').then( m => m.SensorDetailPageModule)
  },
  {
  path: 'sensor-detail/:sensorId/action-thing',
  loadChildren: () => import('./sensor-detail/action-thing/action-thing.module').then( m => m.ActionThingPageModule)
  },
  {
    path: 'sensor-detail/:sensorId/event-thing',
    loadChildren: () => import('./sensor-detail/event-thing/event-thing.module').then( m => m.EventThingPageModule)
    },
  {
    path: 'sensorgraph',
    loadChildren: () => import('./sensorgraph/sensorgraph.module').then( m => m.SensorgraphPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListSensorPageRoutingModule {}
