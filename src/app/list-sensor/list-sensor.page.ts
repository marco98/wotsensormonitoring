import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AotSummaryResolver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { stringify } from 'querystring';
import { map, tap } from 'rxjs/operators';
import { SelectSourcePage } from '../select-source/select-source.page';
import { UtilityService } from '../utility.service';
import { SensorDetail} from './list-sensor-model';
import { SensorTD } from './list-sensor-model-td';

@Component({
  selector: 'app-list-sensor',
  templateUrl: './list-sensor.page.html',
  styleUrls: ['./list-sensor.page.scss'],
})
export class ListSensorPage  {
  private listaSensori:SensorDetail[];
  private listaTD:SensorTD[];
   tokenUtente:string;
  result: any[];
  private urlList:string[];
  private tempValue:SensorDetail;
  constructor(private selectedSource:SelectSourcePage,private http:HttpClient,private apollo:Apollo,private utility:UtilityService) { }

  ngOnInit() {
    this.tokenUtente=this.selectedSource.mytoken;
    const headerDict = {
      'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJja2hlbDZ3MnAwMDAxMWNsY2Y2ZHk2ajF0IiwiaWF0IjoxNjA1NzExOTU3LCJleHAiOjE2MDYzMTY3NTd9.jQd1bDOkNj-KRKFINgr2llwCENSKltO6Tzeqd-Prhtg'
    };
    const queryTD=gql`query{
      viewer{
         things{
           td  
         }
       }
     }`;
     const queryTDURL=gql`query{
      viewer{
         things{
           tdURL 
         }
       }
     }`;
    const executeQueryTD=this.apollo.query({
      query: queryTD,
    })
    const exQueryURL=this.apollo.query({
      query:queryTDURL,
    })
  
    this.urlList=[];
   
    executeQueryTD.subscribe(res=>{console.log(res)
    
      this.result=(res["data"]["viewer"]["things"])
    
    this.listaSensori=[];
    this.listaTD=[]
    exQueryURL.subscribe(res=>{
      console.log(res);
      this.urlList=res["data"]["viewer"]["things"]
    
    console.log(this.result[0].td.forms[0].href);
    for (let i=0;i<=this.result.length;i++){
      
      const obj={
        "titolo":this.result[i].td.title,
        "descrizione":this.result[i].td.description,
        "url":this.urlList[i]["tdURL"],
        "proprieta":[]
      };
      const restObj={
        "titolo":this.result[i].td.title,
        "descrizione":this.result[i].td.description,
        "td":this.result[i].td,
        "proprieta":[]
      }

      console.log(this.result[i].td)
      this.listaTD.push(restObj);
      this.utility.addTD(restObj);
      
      obj.url=obj.url.replace("-test", "");
      this.listaSensori.push(obj);
      this.utility.addSensor(obj);
      
    }
   
    
  })
  const hardSens1={
    "titolo":"DA3VFC99-10-measure",
    "descrizione":"sensore1hardcoded",
    "url":"https://dicam083010.ing.unibo.it:8080/DA3VFC99-10-measure",
    "proprieta":[]
  };
  const hardSens2={
    "titolo":"DA3VFC99-18-measure",
    "descrizione":"sensore2hardcoded",
    "url":"https://dicam083010.ing.unibo.it:8080/DA3VFC99-18-measure",
    "proprieta":[]
  };
  const hardSens4={
    "titolo":"DA3VFC99-18-control",
    "descrizione":"sensore2hardcoded",
    "url":"https://dicam083010.ing.unibo.it:8080/DA3VFC99-18-control",
    "proprieta":[]
  };
  const hardSens3={
    "titolo":"DA3VFC99-10-control",
    "descrizione":"sensorecontrolhardcoded",
    "url":"https://dicam083010.ing.unibo.it:8080/DA3VFC99-10-control",
    "proprieta":[]
  };
  this.listaSensori.push(hardSens1);
  this.listaSensori.push(hardSens2);
  this.listaSensori.push(hardSens3);
  this.listaSensori.push(hardSens4);
  this.utility.addSensor(hardSens1);
  this.utility.addSensor(hardSens2);
  this.utility.addSensor(hardSens3);
  this.utility.addSensor(hardSens4);
    });
    
    //this.tempValue=obj;
  
     
    }
  
  }
  
  
  

    