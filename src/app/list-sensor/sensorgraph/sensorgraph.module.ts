import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SensorgraphPageRoutingModule } from './sensorgraph-routing.module';

import { SensorgraphPage } from './sensorgraph.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule, 
    IonicModule,
    SensorgraphPageRoutingModule
  ],
  declarations: [SensorgraphPage]
})
export class SensorgraphPageModule {}
