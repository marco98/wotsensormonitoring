import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SensorgraphPage } from './sensorgraph.page';

const routes: Routes = [
  {
    path: '',
    component: SensorgraphPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SensorgraphPageRoutingModule {}
