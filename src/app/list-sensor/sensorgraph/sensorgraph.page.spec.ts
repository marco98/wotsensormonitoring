import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SensorgraphPage } from './sensorgraph.page';

describe('SensorgraphPage', () => {
  let component: SensorgraphPage;
  let fixture: ComponentFixture<SensorgraphPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SensorgraphPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SensorgraphPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
