import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { selectionSetMatchesResult } from '@apollo/client/cache/inmemory/helpers';

import Chart from 'chart.js';
import firebase from 'firebase';
import { UtilityService } from 'src/app/utility.service';
import { SensorDetail } from '../list-sensor-model';
import { SensorTD } from '../list-sensor-model-td';
import PouchFind from 'pouchdb-find';
import PouchDB from 'pouchdb';


PouchDB.plugin(PouchFind);
declare let Wot: any


@Component({
  selector: 'app-sensorgraph',
  templateUrl: './sensorgraph.page.html',
  styleUrls: ['./sensorgraph.page.scss'],
})
export class SensorgraphPage  {

  @ViewChild('lineChart') lineChart;
  bars: any=null;
  db:any;
  selected:string;
  created:boolean=false;
  selectedProp:string;
  selectedDateStart:Date;
  selectedDateTo:Date;
  dataextr:any[]
  datearray:any[]
  //ref:any;
  keyForChart:string;
  sensorList:SensorDetail[]
  properties:string[];
  colorArray: any;
  keyCred: string;
  dataclean: any[]
  constructor(
    private utility:UtilityService) {
}

  ionViewWillEnter() {
    
    this.sensorList=this.utility.getAllSensor();
    //this.bars=new Chart(null,null);
  }

   getDataset(){
     if(this.selectedDateTo!=null){
     //if(this.selectedProp!=" "){
     this.dataextr=[];
     this.dataclean=[];
    this.keyCred=this.selected+this.selectedProp;
    this.db=new PouchDB('sensorresults');
    this.db.createIndex({
      index:{
        fields:['sensorNameType']
      }
    })
    const a=this.db.find({
      selector:{
        sensorNameType:this.keyCred.trim()
      }
    })
    console.log(a);
    const sub=a.then((b)=>{
      console.log(b['docs']);
      for(let i=0;i<b['docs'].length;i++){
      console.log(b['docs'][i]);
      this.dataextr.push(b['docs'][i]);
      console.log(this.dataextr)
      this.dataclean.push({
        date:this.dataextr[this.dataextr.length-1].datetime,
        x:this.dataextr[this.dataextr.length-1].x,
        y:this.dataextr[this.dataextr.length-1].y,
        z:this.dataextr[this.dataextr.length-1].z
      })
    }
    console.log(this.dataclean);
   
    this.createLineChart();
   

  }
    

    
    
    );
    
   
   /* const rootRef=this.db.collection("sensorresults");
    const query=rootRef.where("sensorNameType","==",this.keyCred);
   const a= query.get()
    .then(querySnap=>{
      querySnap.forEach((doc)=>{
        this.ref=doc.data();
        console.log(doc.data())
      });
    }).catch((error) => {
      console.log("Error getting documents: ", error);
  }).finally(()=>console.log(this.ref));
*/
//}
/*else{
  alert("please select a property")
}*/

     }
  
}
createStuff(){
      
}



  createLineChart() {
    //this.removeData(this.lineChart)
    this.created=true;
    const datelabel=[];
    this.dataclean.sort((a,b)=>(a.date > b.date)?1:-1)
    this.dataclean.forEach(doc => {
      if (this.isInInterval(new Date(doc.date))){
      
      datelabel.push((new Date(doc.date)).getDate()+" "+((new Date(doc.date)).getMonth()+1)+" "+(new Date(doc.date)).getFullYear())
      }
    });
  
    const xData=[];
    this.dataclean.forEach(doc => {
      if (this.isInInterval(new Date(doc.date))){
      xData.push(doc.x)
      }
    });
    const yData=[];
    this.dataclean.forEach(doc => {
      if (this.isInInterval(new Date(doc.date))){
      yData.push(doc.y)
      }
    });
    const zData=[];
    this.dataclean.forEach(doc => {
      if (this.isInInterval(new Date(doc.date))){
      zData.push(doc.z)
      }
    });

    

    this.bars = new Chart(this.lineChart.nativeElement, {
      type: 'line',
      data: {
        labels: datelabel,
        datasets: [{
          label: 'x',
          fill: false,
          data: xData,
          backgroundColor: 'rgb(38, 194, 129)', // array should have same number of elements as number of dataset
          borderColor: 'rgb(38, 194, 129)',// array should have same number of elements as number of dataset
          borderWidth: 1
        },
        {
          label: 'y',
          fill: false,
          data: yData,
          backgroundColor: 'rgb(255,0,0)', // array should have same number of elements as number of dataset
          borderColor: 'rgb(255,0,0)',// array should have same number of elements as number of dataset
          borderWidth: 1
        },
        {
          label: 'z',
          fill: false,
          data: zData,
          backgroundColor: 'rgb(25, 89,100)', // array should have same number of elements as number of dataset
          borderColor: 'rgb(25, 89,100)',// array should have same number of elements as number of dataset
          borderWidth: 1
        }],
      },
    });
    if (this.bars!=null){
    this.selectedDateStart=null;
    this.selectedDateTo=null;
    this.selectedProp=null;
    this.selected=null;
    }
 
  
  }

  loadProperties(sensor:string){
    if(this.selected!=null){
    if(this.created){
    this.removeData(this.bars)
    }
    
    console.log(this.selected);
    console.log(sensor);

    var url=this.utility.findUrl(sensor.trim());
    var servient = new Wot.Core.Servient();
servient.addClientFactory(new Wot.Http.HttpsClientFactory());
this.keyCred="urn:dev:"+sensor;
servient.addCredentials({
  [this.keyCred]: {
      "username": "user",
      "password": "pass"  // if you copy these and don't change them, don't claim you were "hacked"
  }

});
var helpers = new Wot.Core.Helpers(servient);
      servient.start().then((thingFactory)=> {
        helpers.fetch(url).then((td) =>{
          thingFactory.consume(td)
        .then((thing) => {
          console.log(thing);
          td=thing.getThingDescription();
          this.properties=[" "];
          for ( let property in td.properties ) {
            if (td.properties.hasOwnProperty(property)) {
              this.properties.push(property);
              
              console.log(property);
            }}
           
          });
        }).catch((error) => {
          window.alert("Could not fetch TD.\n" + error)
        })
      })
    }
    }

  
  removeData(chart) {
    /*
      chart.data.labels.pop();
      chart.data.datasets.forEach((dataset) => {
        
          dataset.data.pop();
      });
      chart.update();
      */
     chart.destroy()
  }

  isInInterval(datedoc:Date) {
    if (((new Date(datedoc))<=(new Date(this.selectedDateTo))) &&  ((new Date(datedoc)>= new Date(this.selectedDateStart)))) {
      return true
    }else{
      return false
    }
  }
  cleanSelection(){
    this.selectedDateStart=null;
    this.selectedDateTo=null;
    this.selectedProp=null;
    this.selected=null;
    this.bars.destroy();
  }
  
  
  }
