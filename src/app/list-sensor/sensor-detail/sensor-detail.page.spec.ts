import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SensorDetailPage } from './sensor-detail.page';

describe('SensorDetailPage', () => {
  let component: SensorDetailPage;
  let fixture: ComponentFixture<SensorDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SensorDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SensorDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
