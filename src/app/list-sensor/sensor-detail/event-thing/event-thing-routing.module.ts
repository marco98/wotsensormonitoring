import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventThingPage } from './event-thing.page';

const routes: Routes = [
  {
    path: '',
    component: EventThingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EventThingPageRoutingModule {}
