import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'src/app/utility.service';

@Component({
  selector: 'app-event-thing',
  templateUrl: './event-thing.page.html',
  styleUrls: ['./event-thing.page.scss'],
})
export class EventThingPage {
private currentTd
private eventSubscription:any={}
private currentThing;
private eventList:string[]
private toggleState:boolean[]
//private eventSubscriptions :Observable<any[]>
  constructor(private utility:UtilityService) { }

  ionViewWillEnter() {
    //this.currentThing=this.utility.getcurrentThing();
    this.eventList=[]
    this.toggleState=[]
    //this.currentTd=this.utility.findTD(paramMap.get("sensorId"))
    this.currentTd=this.utility.getcurrentTd()
    for (let evnt in this.currentTd.events){
      if (this.currentTd.events.hasOwnProperty(evnt)) {
      this.eventList.push(evnt)
      this.toggleState.push(false)
      this.eventSubscription[evnt]=false
      }
    }
    
  }

  

  eventToggle(toggleState,event){

    if (toggleState && !this.eventSubscription[event] && this.eventSubscription[event]===false) {
      /*window.alert("Event subscribes")
      eventSubscriptions[event] = this.utility.getcurrentThing().events[event].subscribeEvent(
        (response) => { window.alert("Event " + event + " detected\nMessage: " + response); },
        (error) => { window.alert("Event " + event + " error\nMessage: " + error); }
      )
    } else if (!toggleState && eventSubscriptions[event]) {
      
      eventSubscriptions[event].unsubscribe().then(()=>window.alert("Event unsubscribes"));
      window.alert("Event unsubscribes")
      */
      this.eventSubscription[event] = true;
      this.utility.getcurrentThing().subscribeEvent(event, function (data) {
        console.log("Data:" + data);
        //updateProperties();
      })
      .then(()=> {
        // OK
        window.alert("Event subscribed")
      })
      .catch((error) => {  window.alert("Event " + event + " error\nMessage: " + error); })
      ;
    }else if (!toggleState && this.eventSubscription[event]) {
      console.log("Try to unsubscribing for event: " + event);
      this.eventSubscription[event] = false;
      this.utility.getcurrentThing().unsubscribeEvent(event)
      .then(()=> {
        // OK
      })
      .catch((error) => {  window.alert("Event unsubscribed"); });
      
    }
 
  }

}
