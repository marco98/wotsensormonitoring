import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EventThingPage } from './event-thing.page';

describe('EventThingPage', () => {
  let component: EventThingPage;
  let fixture: ComponentFixture<EventThingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventThingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EventThingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
