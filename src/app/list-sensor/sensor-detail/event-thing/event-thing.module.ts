import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EventThingPageRoutingModule } from './event-thing-routing.module';

import { EventThingPage } from './event-thing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EventThingPageRoutingModule
  ],
  declarations: [EventThingPage]
})
export class EventThingPageModule {}
