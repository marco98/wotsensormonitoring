import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ActionThingPage } from './action-thing.page';

describe('ActionThingPage', () => {
  let component: ActionThingPage;
  let fixture: ComponentFixture<ActionThingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionThingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ActionThingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
