import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActionThingPage } from './action-thing.page';

const routes: Routes = [
  {
    path: '',
    component: ActionThingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActionThingPageRoutingModule {}
