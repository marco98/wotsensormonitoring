import { Component} from '@angular/core';
//import { JsonEditorComponent, JsonEditorOptions } from 'ang-jsoneditor';
//import JSONEditor from 'jsoneditor';
//@import "~jsoneditor/dist/jsoneditor.min.css"
import { UtilityService } from 'src/app/utility.service';
import { SensorDetail } from '../../list-sensor-model';
import { SensorDetailPage } from '../sensor-detail.page';
declare let JSONEditor:any;
@Component({
  selector: 'app-action-thing',
  templateUrl: './action-thing.page.html',
  styleUrls: ['./action-thing.page.scss'],
})

export class ActionThingPage {
  private actionList:string[];
  private currentThing;
  private hideButton=false;
  private editor;
  private actualAction:any;
  constructor(private sensorDetail:SensorDetailPage,private utility:UtilityService) { }

  ionViewWillEnter(){
    this.currentThing=this.utility.getcurrentThing();
    this.actionList=[];
    for ( let action in this.utility.getcurrentTd().actions ) {
      if (this.utility.getcurrentTd().actions.hasOwnProperty(action)) {
      this.actionList.push(action);
        console.log(action);
      }
    }}
       /* item.onclick = (click) => { 
          this.showSchemaEditor(action, this.utility.getcurrentThing() )
        }
      }
    };
  }*/

  showSchemaEditor(action, thing) {
    this.actualAction=action;
    let td = thing.getThingDescription();
    // Remove old editor
    this.removeSchemaEditor()
  
    let placeholder = document.getElementById('editor_holder');
    if (td.actions[action] && td.actions[action].input ) {  
      td.actions[action].input.title = action
      this.editor = new JSONEditor(
        placeholder, 
        {	schema: td.actions[action].input,
          name: action,
          mode:'form',
          iconlib: "fontawesome4",
        mainMenuBar:false
           }
        
        
      );
    }
    this.hideButton=true;
    // Add invoke button
   /* let button = document.createElement("button")
    button.appendChild(document.createTextNode("Invoke"))
    placeholder.appendChild(button)*/
    
    
   /* button.onclick = () => { 
      let input = this.editor ? this.editor.getValue() : "";
      thing.invokeAction(action, input)
      .then((res) => { 
        if (res) {
          window.alert("Success! Received response: " + res)
        } else {
          window.alert("Executed successfully.")
        }
      })
      .catch((err) => { window.alert(err) })
      this.removeSchemaEditor()
    };*/
  }

  invoke(editor,action,thing){
    let input = editor ? editor.getValue() : "";
    thing.invokeAction(action, input)
    .then((res) => { 
      if (res) {
        window.alert("Action executed: the response is:" + res)
      } else {
        window.alert("Action executed.")
      }
    })
    .catch((err) => { window.alert("Cannot execute the action"+err) })
    this.removeSchemaEditor()
  }

  removeSchemaEditor() {
    let placeholder = document.getElementById('editor_holder');
    while (placeholder.firstChild){
        placeholder.removeChild(placeholder.firstChild);
    }
  }

}
