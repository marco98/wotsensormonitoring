import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
declare let Wot: any
@Component({
  selector: 'app-modalwriteproperty',
  templateUrl: './modalwriteproperty.page.html',
  styleUrls: ['./modalwriteproperty.page.scss'],
})
export class ModalwritepropertyPage  {
  @Input() property: any;
  @Input() thing: any;
  private valueEntered:any;
  constructor(private modalController:ModalController) { }

  ionViewWillEnter() {

  }
  writeProp(){
    this.thing.writeProperty(this.property,this.valueEntered)
    this.dismiss();
  }

  dismiss(){
    this.modalController.dismiss({
      'dismissed': true
    });
  }

}
