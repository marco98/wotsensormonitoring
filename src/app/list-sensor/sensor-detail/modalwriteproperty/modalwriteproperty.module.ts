import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalwritepropertyPageRoutingModule } from './modalwriteproperty-routing.module';

import { ModalwritepropertyPage } from './modalwriteproperty.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalwritepropertyPageRoutingModule
  ],
  declarations: [ModalwritepropertyPage]
})
export class ModalwritepropertyPageModule {}
