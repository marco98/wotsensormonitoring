import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalwritepropertyPage } from './modalwriteproperty.page';

describe('ModalwritepropertyPage', () => {
  let component: ModalwritepropertyPage;
  let fixture: ComponentFixture<ModalwritepropertyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalwritepropertyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalwritepropertyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
