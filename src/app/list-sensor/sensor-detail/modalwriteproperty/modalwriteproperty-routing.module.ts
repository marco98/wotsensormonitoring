import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalwritepropertyPage } from './modalwriteproperty.page';

const routes: Routes = [
  {
    path: '',
    component: ModalwritepropertyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalwritepropertyPageRoutingModule {}
