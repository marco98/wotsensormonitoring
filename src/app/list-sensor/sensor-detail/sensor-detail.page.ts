import { Component, Injectable, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
//import { randomInt } from 'crypto';
import { Subscription } from 'rxjs';
import { SensorResults } from 'src/app/models/sensorResults.model';
import { UtilityService } from 'src/app/utility.service';
import { SensorDetail } from '../list-sensor-model';
import { ModalController } from '@ionic/angular';
import { ModalwritepropertyPage } from './modalwriteproperty/modalwriteproperty.page';
import * as PouchDB from 'pouchdb/dist/pouchdb';

declare let Wot: any
@Component({
  selector: 'app-sensor-detail',
  templateUrl: './sensor-detail.page.html',
  styleUrls: ['./sensor-detail.page.scss'],
})
@Injectable()
export class SensorDetailPage  {
  private propertiesList:string[];
  sensorresults={} as SensorResults
  private sensorName:string;
  private thing;
  private currentTd;
  private myTd;
  private keyCred;
  private value;
  currentUrl:string
   sensor:SensorDetail;
  private popup:boolean;
  db: any;
 
  constructor( private modalController:ModalController, private route: ActivatedRoute,private navCtrl: NavController,private utility:UtilityService,private toastCtrl:ToastController,private firestore:AngularFirestore, private loadingCntrl:LoadingController) { }

  ionViewWillEnter() {
    
    this.popup=false;
    this.route.paramMap.subscribe((paramMap) => {
      if (!paramMap.has("sensorId")) {
        this.navCtrl.navigateBack("list-sensor");
        return;
      }
      this.currentUrl = this.utility
        .findUrl(paramMap.get("sensorId"))
        this.sensorName=paramMap.get("sensorId")
        //this.currentTd=this.utility.findTD(paramMap.get("sensorId"))
        });

        
      console.log(this.currentTd);
       //scommenta quando usi le API this.keyCred=/*"urn:dev:"+this.sensorName;*/this.currentTd["id"]
       this.keyCred="urn:dev:"+this.sensorName

     var servient = new Wot.Core.Servient();
servient.addClientFactory(new Wot.Http.HttpsClientFactory());

servient.addCredentials({
    [this.keyCred]: {
        "username": "user",
        "password": "pass"  // if you copy these and don't change them, don't claim you were "hacked"
    }

  });

var helpers = new Wot.Core.Helpers(servient);
 this.get_td(this.currentUrl,/*this.currentTd*/servient,helpers); 
 }

 
 
//interrogazione API
/*
 get_td(td,servient,helpers) {
	servient.start().then((thingFactory)=> {
			thingFactory.consume(td)
		.then((thing) => {
      console.log(thing);
      td=thing.getThingDescription();
      this.utility.setcurrentTd(td);
      this.utility.setcurrentThing(thing);
      this.thing=thing;
      this.propertiesList=[];
      for ( let property in td.properties ) {
        if (td.properties.hasOwnProperty(property)) {
          this.propertiesList.push(property);
          console.log(property);
        }}
			});
		}).catch((error) => {
			window.alert("Could not fetch TD.\n" + error)
		})
	}*/

  //INTERROGAZIONE PER TEST 
  get_td(addr,servient,helpers) {
    servient.start().then((thingFactory)=> {
      helpers.fetch(addr).then((td) =>{
        thingFactory.consume(td)
      .then((thing) => {
        console.log(thing);
        td=thing.getThingDescription();
        this.utility.setcurrentTd(td);
        this.utility.setcurrentThing(thing);
        this.thing=thing;
        this.propertiesList=[];
        for ( let property in td.properties ) {
          if (td.properties.hasOwnProperty(property)) {
            this.propertiesList.push(property);
            
            console.log(property);
          }}
          this.utility.addProperties(this.propertiesList,this.sensorName);
        });
      }).catch((error) => {
        window.alert("Error, thing description not fetchable:" + error)
      })
    })}

  





onClickProp(property){
 // console.log(this.utility.getcurrentTd()['properties'][property]['forms'][0]['htv:methodName'])
  if((this.utility.getcurrentTd()['properties'][property]['forms'][0]['htv:methodName'])=='PUT'){
    this.presentModal(property,this.thing);
   /* this.thing.writeProperty(property,2).then(res=>{
      window.alert("Property wrote")
    })*/
  }else{
	this.thing.readProperty(property)
				.then(res =>{
         // console.log("DATI DEL SENSORE"+res['sosa:hasResult'].x),
          this.value=property + ": " + JSON.stringify(res),
          /*//dati reali
          this.sensorresults.sensorName=this.sensorName,
          this.sensorresults.datetime=new Date(),
          this.sensorresults.sensorType=property,
          this.sensorresults.sensorNameType=this.sensorName+property,
          this.sensorresults.x=res['sosa:hasResult'].x,
          this.sensorresults.y=res['sosa:hasResult'].y,
          this.sensorresults.z=res['sosa:hasResult'].z,
          */
         //dati mock
         this.sensorresults.sensorName=this.sensorName,
         this.sensorresults.datetime=new Date(),
         this.sensorresults.sensorType=property,
         this.sensorresults.sensorNameType=this.sensorName+property,
         this.sensorresults.x=Math.random(),
         this.sensorresults.y=Math.random(),
         this.sensorresults.z=Math.random(),
         this.addtoPouchDB(this.sensorresults)
				.catch(err => window.alert("error: cannot send data to database " + err))

        })
      }
}

async addtoPouchDB(sensorresults:SensorResults){
  this.db=new PouchDB('sensorresults');
let loader=this.loadingCntrl.create({
  message:"Please wait..."
});
  (await loader).present();
  try{
    await this.db.post(sensorresults);

  }catch(e){
    this.showToast(e)
  }
  (await loader).dismiss();

}
  showToast(message: string) {
    this.toastCtrl.create({
      message:message,
      duration:3000
    }).then(toastData=>toastData.present());
  }



get retriveTd(){
  console.log(this.myTd);
  return this.myTd;
}
get retriveThing(){
  console.log(this.thing);
  return this.thing;
}
async presentModal(property,thing){
  const modal = await this.modalController.create({
    component: ModalwritepropertyPage,
    componentProps: {
      'property': property,
      'thing': thing
      
    }

  });
  return await modal.present();
}

}



  


