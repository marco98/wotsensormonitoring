import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectSourcePageRoutingModule } from './select-source-routing.module';

import { SelectSourcePage } from './select-source.page';
import { GraphQLModule } from '../graphql.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectSourcePageRoutingModule,
    
  ],
  declarations: [SelectSourcePage]
})
export class SelectSourcePageModule {}
