import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { Component, Injectable, NgModule, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { createHttpLink } from '@apollo/client/link/http/createHttpLink';
import { delay, map, switchMap, take, tap } from "rxjs/operators";
import { Apollo, gql } from 'apollo-angular';
import { BehaviorSubject } from 'rxjs';
import { get } from 'http';
import { url } from 'inspector';
import {ApolloClientOptions, ApolloLink, InMemoryCache} from '@apollo/client/core';
import {HttpLink} from 'apollo-angular/http';
import { Router } from '@angular/router';
import { GraphQLModule } from '../graphql.module';
import { setContext } from 'apollo-link-context';


//TODO: GESTIRE MESSAGGIO ERRORE LOGIN
@NgModule({
  exports: [
    HttpClientModule,
  ]
})


@Component({
  selector: 'app-select-source',
  templateUrl: './select-source.page.html',
  styleUrls: ['./select-source.page.scss'],
})
@Injectable({
  providedIn:'root',
})


export class SelectSourcePage implements OnInit {
  option: string;
   ishidden = true;
   sourceSelected:string;
   private _url=new BehaviorSubject<any>('');
   private username:string;
   private password:string;
   mytoken:string;
   private logged=false;
   private apolloCreated=false;
   
   get url(){
     return this._url.asObservable();
   }

  constructor(private http: HttpClient,private apollo:Apollo,
    private httpLink:HttpLink,private router:Router,private graph:GraphQLModule)
     { }

  ngOnInit() {
    
    
  }
  ionViewWillEnter(){
    
    
  }

  onSubmit(form:NgForm){
    
    if(!form.valid){
      return;
    }
    
    this.apollo.removeClient();
    this.username=form.value.mail;
    this.password=form.value.password;
    this._url.next(this.sourceSelected);
    if(this.apolloCreated){
      this.afterSubmit();
    }else{
      this.createApollo();
      
    }
    
    }
   
  

  createApollo(){
    const basic = setContext((operation, context) => ({
      headers: {
        Accept: 'charset=utf-8'
      }
    }));
  
    // Get the authentication token from local storage if it exists
    const token = localStorage.getItem('token');
    const auth = setContext((operation, context) => ({
      headers: {
        Authorization: `Bearer ${token}`
      },
    }));
  
    this.apolloCreated=true;
    this.url.subscribe(res=>{
      console.log(res+"aaa");
      const uri=res;
      this.apollo.create({
        link: ApolloLink.from([(basic as unknown) as ApolloLink, (auth as unknown) as ApolloLink, this.httpLink.create({ uri })]),
        cache: new InMemoryCache(),
        resolvers: {
          Post: {
            async isPublic(r, args, ctx) {
              console.log({r, args, ctx});
    
              return new Promise(resolve => {
                setTimeout(() => {
                  resolve(true)
                }, 500)
              })
            }
          }
        }
      });
      
      
    })
    this.afterSubmit();
  }
    
  
  selectSource(){
    this.apolloCreated=false;
    console.log(this.option);
    this.sourceSelected=this.option;
    if(this.option){
      this.ishidden=false;
    }
  }

  afterSubmit(){
   
    const loginMutuation=gql`mutation($user:String!, $password:String!){
      login(data:{usernameOrEmail:$user,password:$password}){
        token
      }
    }`;
   this.apollo.mutate({
      mutation:loginMutuation,
      variables:{
        user:this.username,
        password:this.password
      },
    }).subscribe(data => {
      console.log(data);
      for (const token in data){
        this.mytoken=(data[token].login.token)
      }
     
      this.logged=true;
      console.log(this.mytoken);
      if(this.logged){
        localStorage.setItem('token',this.mytoken);
      this.router.navigateByUrl("list-sensor")

      }
    });
  }

}
