import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectSourcePage } from './select-source.page';

describe('SelectSourcePage', () => {
  let component: SelectSourcePage;
  let fixture: ComponentFixture<SelectSourcePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSourcePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectSourcePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
